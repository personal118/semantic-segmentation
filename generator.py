import numpy as np                   # advanced math library
import matplotlib.pyplot as plt      # MATLAB like plotting routines
import random                        # for generating random numbers
import gc

from keras.datasets import mnist     # MNIST dataset is included in Keras
from keras.models import Sequential  # Model type to be used

from keras.layers.core import Dense, Dropout, Activation # Types of layers to be used in our model
from keras.utils import np_utils                         # NumPy related tools

from segmentation_dataset_generator import create_semantic_segmentation_data_from_digits
from segmentation_dataset_generator.utils import add_noise_to_dataset
from PIL import Image

# The MNIST data is split between 60,000 28 x 28 pixel training images and 10,000 28 x 28 pixel images
(X_mnist_train, y_mnist_train), (X_mnist_test, y_mnist_test) = mnist.load_data()

def transformation(input_element):
    min_scale_factor = 1
    max_scale_factor = 1.5
    min_rotate_arg = -25
    max_rotate_arg = 25
    x = input_element.reshape((input_element.shape[0], input_element.shape[1]))
    new_size = (np.random.uniform(low=min_scale_factor, high=max_scale_factor) * np.array(x.shape)).astype(int)
    new_array = np.array(Image.fromarray(x).resize(size=new_size).rotate(np.random.uniform(low=min_rotate_arg, high=max_rotate_arg)))
    return new_array.reshape((new_array.shape[0], new_array.shape[1], 1))

def generate_mnist_dataset(X_test, y_test, num):
    X_test_reshaped = X_test.reshape((len(X_test), 28, 28, 1)) / 255
    X_test_reshaped[X_test_reshaped < 0.2] = 0
    input_data, target_data, overlaids, labels = create_semantic_segmentation_data_from_digits(
        digits=X_test_reshaped, 
        digit_labels=y_test,
        num_samples=num, 
        image_shape=(120,120),
        min_num_digits_per_image=0,
        max_num_digits_per_image=8,
        num_classes=10, 
        max_iou=0.1,
        labels_are_exclusive=False,
        target_is_whole_bounding_box=False,
        transformation=transformation,
    )
    input_data = add_noise_to_dataset(input_data)
    return input_data, target_data

# dirname = "./datasets/"
# # for i in range(1,4):
# #     X_train, y_train = generate_mnist_dataset(X_mnist_train, y_mnist_train, i*5000)
# #     np.save(f'{dirname}X_seg_mnist_train_{i}.npy', X_train)
# #     np.save(f'{dirname}y_seg_mnist_train_{i}.npy', y_train)
# #     del X_train
# #     del y_train
# #     gc.collect()

# X_test, y_test = generate_mnist_dataset(X_mnist_test, y_mnist_test, 1000)
# np.save(f'{dirname}X_seg_mnist_test.npy', X_test)
# np.save(f'{dirname}y_seg_mnist_test.npy', y_test)