import numpy as np                   # advanced math library
import matplotlib.pyplot as plt      # MATLAB like plotting routines
import random                        # for generating random numbers

from keras.datasets import mnist     # MNIST dataset is included in Keras
from keras.models import Sequential  # Model type to be used

from keras.layers.core import Dense, Dropout, Activation # Types of layers to be used in our model
from keras.utils import np_utils                         # NumPy related tools

from segmentation_dataset_generator import create_semantic_segmentation_data_from_digits
from PIL import Image

def add_noise_to_dataset(input_data):
    new_dataset = []
    for index, i in enumerate(input_data):
        if index % 100 == 0:
            print(index)
        new_dataset.append(add_noise_background(i))
    return np.array(new_dataset)

def add_noise_background(input_data):
    array = np.random.uniform(low=0.3, high=1, size=input_data.shape)
    array =  np.reshape(array, input_data.shape)
    tmp = input_data
    tmp[tmp == 0] = 10 
    tmp = tmp / tmp.max()
    tmp[tmp == 1] = array[tmp == 1]
    return tmp
