image_url = "https://picsum.photos/256/256"

import requests

for i in range(0, 100):
	img_data = requests.get(image_url).content
	with open(f'./backgrounds/{i}.jpg', 'wb') as handler:
		handler.write(img_data)